from django.contrib import admin
from .models import User, Menu, Dishes, Ingredients

admin.site.register(User)
admin.site.register(Menu)
admin.site.register(Dishes)
admin.site.register(Ingredients)


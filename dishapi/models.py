from django.db import models
from django.contrib.auth.models import User

class User(models.Model):
    username = models.CharField(max_length=50, blank=False)
    password = models.CharField(max_length=50, blank=False)
    email = models.EmailField(blank=False)
    
    def __str__(self):
        return self.username

class Menu(models.Model):
    title = models.CharField(max_length=50, blank=False)

    def __str__(self):
        return self.title

class Dishes(models.Model): 
    title = models.CharField(max_length=50)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.title

class Ingredients(models.Model):
    title = models.CharField(max_length=50)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
    




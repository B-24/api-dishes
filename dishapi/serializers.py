from rest_framework import serializers
from .models import User, Menu, Dishes, Ingredients


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User 
        fields = ['username', 'password', 'email']


class MenuSerializer(serializers.ModelSerializer):
    class Meta:
        model = Menu
        fields = ['title']


class DishesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dishes
        fields = ['title']        


class IngredientsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ingredients
        fields = ['title']

